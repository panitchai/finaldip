public class MyEncrypt {
    protected String origin;
    protected String pattern;
    protected String widths;

    protected String startpattern = "1101";
    protected String endpattern = "1011";
    private final String startwidths = "211";
    private final String endwidths = "112";
    protected String[][] encrypttable = { 
        {   " " ,   "11011001100"   ,   "212222"    },
        {	"!"	,	"11001101100"	,	"222122"	},
        {	"\"",	"11001100110"	,	"222221"	},
        {	"#"	,	"10010011000"	,	"121223"	},
        {	"$"	,	"10010001100"	,	"121322"	},
        {	"%"	,	"10001001100"	,	"131222"	},
        {	"&"	,	"10011001000"	,	"122213"	},
        {	"\'",	"10011000100"	,	"122312"	},
        {	"("	,	"10001100100"	,	"132212"	},
        {	")"	,	"11001001000"	,	"221213"	},
        {	"*"	,	"11001000100"	,	"221312"	},
        {	"+"	,	"11000100100"	,	"231212"	},
        {	","	,	"10110011100"	,	"112232"	},
        {	"-"	,	"10011011100"	,	"122132"	},
        {	"."	,	"10011001110"	,	"122231"	},
        {	"/"	,	"10111001100"	,	"113222"	},
        {	"0"	,	"10011101100"	,	"123122"	},
        {	"1"	,	"10011100110"	,	"123221"	},
        {	"2"	,	"11001110010"	,	"223211"	},
        {	"3"	,	"11001011100"	,	"221132"	},
        {	"4"	,	"11001001110"	,	"221231"	},
        {	"5"	,	"11011100100"	,	"213212"	},
        {	"6"	,	"11001110100"	,	"223112"	},
        {	"7"	,	"11101101110"	,	"312131"	},
        {	"8"	,	"11101001100"	,	"311222"	},
        {	"9"	,	"11100101100"	,	"321122"	},
        {	":"	,	"11100100110"	,	"321221"	},
        {	";"	,	"11101100100"	,	"312212"	},
        {	"<"	,	"11100110100"	,	"322112"	},
        {	"="	,	"11100110010"	,	"322211"	},
        {	">"	,	"11011011000"	,	"212123"	},
        {	"?"	,	"11011000110"	,	"212321"	},
        {	"@"	,	"11000110110"	,	"232121"	},
        {	"A"	,	"10100011000"	,	"111323"	},
        {	"B"	,	"10001011000"	,	"131123"	},
        {	"C"	,	"10001000110"	,	"131321"	},
        {	"D"	,	"10110001000"	,	"112313"	},
        {	"E"	,	"10001101000"	,	"132113"	},
        {	"F"	,	"10001100010"	,	"132311"	},
        {	"G"	,	"11010001000"	,	"211313"	},
        {	"H"	,	"11000101000"	,	"231113"	},
        {	"I"	,	"11000100010"	,	"231311"	},
        {	"J"	,	"10110111000"	,	"112133"	},
        {	"K"	,	"10110001110"	,	"112331"	},
        {	"L"	,	"10001101110"	,	"132131"	},
        {	"M"	,	"10111011000"	,	"113123"	},
        {	"N"	,	"10111000110"	,	"113321"	},
        {	"O"	,	"10001110110"	,	"133121"	},
        {	"P"	,	"11101110110"	,	"313121"	},
        {	"Q"	,	"11010001110"	,	"211331"	},
        {	"R"	,	"11000101110"	,	"231131"	},
        {	"S"	,	"11011101000"	,	"213113"	},
        {	"T"	,	"11011100010"	,	"213311"	},
        {	"U"	,	"11011101110"	,	"213131"	},
        {	"V"	,	"11101011000"	,	"311123"	},
        {	"W"	,	"11101000110"	,	"311321"	},
        {	"X"	,	"11100010110"	,	"331121"	},
        {	"Y"	,	"11101101000"	,	"312113"	},
        {	"Z"	,	"11101100010"	,	"312311"	},
        {	"["	,	"11100011010"	,	"332111"	},
        {	"\\",	"11101111010"	,	"314111"	},
        {	"]"	,	"11001000010"	,	"221411"	},
        {	"^"	,	"11110001010"	,	"431111"	},
        {	"_"	,	"10100110000"	,	"111224"	},
    };

    public String characterToPattern(String mycharacter){
        mycharacter = mycharacter.toUpperCase();
        String mypattern = "";
        boolean ismatch =false;
        for(int i=0 ;i< encrypttable.length;i++){
            if( !(ismatch) && mycharacter.equals(encrypttable[i][0]) ){
                mypattern = encrypttable[i][1];
                ismatch = true;
            }
        }
        return mypattern;
    }
    public String characterToWidths(String mycharacter){
        mycharacter = mycharacter.toUpperCase();
        String mypattern = "";
        boolean ismatch =false;
        for(int i=0 ;i< encrypttable.length;i++){
            if( !(ismatch) && mycharacter.equals(encrypttable[i][0]) ){
                mypattern = encrypttable[i][2];
                ismatch = true;
            }
        }
        return mypattern;
    }
    public String patternTocharacter(String mypattern){
        mypattern = mypattern.toUpperCase();
        String mycharacter = "";
        boolean ismatch =false;
        for(int i=0 ;i< encrypttable.length;i++){
            if( !(ismatch) && mypattern.equals(encrypttable[i][1]) ){
                mycharacter = encrypttable[i][0];
                ismatch = true;
            }
        }
        return mycharacter;
    }
    public String patternToWidths(String mypattern){
        mypattern = mypattern.toUpperCase();
        String mywidths = "";
        boolean ismatch =false;
        for(int i=0 ;i< encrypttable.length;i++){
            if( !(ismatch) && mypattern.equals(encrypttable[i][1]) ){
                mywidths = encrypttable[i][2];
                ismatch = true;
            }
        }
        return mywidths;
    }
    public String widthsTocharacter(String mywidths){
        mywidths = mywidths.toUpperCase();
        String mycharacter = "";
        boolean ismatch =false;
        for(int i=0 ;i< encrypttable.length;i++){
            if( !(ismatch) && mywidths.equals(encrypttable[i][2]) ){
                mycharacter = encrypttable[i][0];
                ismatch = true;
            }
        }
        return mycharacter;
    }
    public String widthsTopatten(String mywidths){
        mywidths = mywidths.toUpperCase();
        String mypattern = "";
        boolean ismatch =false;
        for(int i=0 ;i< encrypttable.length;i++){
            if( !(ismatch) && mywidths.equals(encrypttable[i][1]) ){
                mypattern = encrypttable[i][2];
                ismatch = true;
            }
        }
        return mypattern;
    }

    public Boolean characterToPatternAll(){
        origin = origin.toUpperCase();
        pattern = "";
        String tem ="";
        for (int i=0;i<origin.length();i++){
            tem = origin.substring(i, i+1);
            pattern = pattern +""+ characterToPattern(tem);
        }
        return true;
    }
    public Boolean characterTowidthsAll(){
        origin = origin.toUpperCase();
        widths = "";
        String tem ="";
        for (int i=0;i<origin.length();i++){
            tem = origin.substring(i, i+1);
            widths = widths +""+ characterToWidths(tem);
        }
        return true;
    }
    public Boolean encryptPattern(){
        origin = origin.toUpperCase();
        pattern = ""+startpattern;
        String tem ="";
        for (int i=0;i<origin.length();i++){
            tem = origin.substring(i, i+1);
            pattern = pattern +""+ characterToPattern(tem);
        }
        pattern = pattern +""+ endpattern;
        return true;
    }
    public Boolean encryptwidths(){
        origin = origin.toUpperCase();
        widths = ""+startwidths;
        String tem ="";
        for (int i=0;i<origin.length();i++){
            tem = origin.substring(i, i+1);
            widths = widths +""+ characterToWidths(tem);
        }
        widths = widths +""+ endwidths;
        return true;
    }

    public String searchpattern(String fullPattern){
        boolean isstartpattern=false;
        boolean isendpattern=false;
        int tem =0;
        int sizewidth =0;
        int count =0;
        int state =1;
        int temstate1 =0;
        int temstate2 =0;
        String mypattern="";
        
        for (int i=0;i< fullPattern.length();i++){
            tem = Integer.parseInt(fullPattern.substring(i, i+1));

            if(isstartpattern == false){
                switch (state) {
                    case 1:
                    if(tem==1){
                        count++;
                    }
                    else {
                        state=2;
                        temstate1=count;
                        count=0;
                    }   
                        
                    break;
                    
                    
                    case 2: if(tem ==0){
                        count++;

                    }
                    else {
                        temstate2=count;
                        int min = (int)(temstate1 *0.5*0.2);
                        int max = (int)(temstate1 +0.5*0.2);
                        if(temstate2 > min && temstate2 <max){
                            sizewidth = tem;
                            count =0;
                            isstartpattern=true;
                        }
                        else {
                            count =0;
                            temstate1=0;
                            temstate2=0;
                            state=1;
                        }

                    }
                        
                        break;
                
                    default:
                        break;
                }

            }
            else if (isendpattern == false){
                switch (state) {
                    case 1:
                    if(tem==1){
                        count++;


                    }
                    else{
                        if(count >sizewidth *0.8 && count < sizewidth *1.2){
                            count =0;
                            state =2;
                        }
                        else {
                            count =0;
                        }

                    }
                        break;
                    case 2:
                    if(tem==0){
                        count++;
                    }
                    else {
                        if(count >sizewidth *0.8 && count < sizewidth *1.2){
                            count =0;
                            state =1;
                            isendpattern =true;
                        }
                        else {
                            count =0;
                            state =1;
                        }

                    }
                    break;


                
                    default:
                        break;
                }
            }
            else {
            }
        }
        if (isstartpattern && isendpattern) mypattern ="******* \n";
        return mypattern;

    }

    public String getorigin(){ return origin;}
    public String getpattern(){ return pattern;}
    public String getwidths(){ return widths;}
    public void setorigin(final String neworigin){origin = neworigin;}
    public void setpattern(final String newpattern){pattern = newpattern;}
    public void setwidths(final String newwidths){widths = newwidths;}
}