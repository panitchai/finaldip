import java.util.Scanner;
import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.awt.image.BufferedImage;

public class run {
    public static void main(String[] args) {
        Imagemanager im = new Imagemanager();
        im.reaad("Mission_3.jpg");
        //im.averagingFilter(9);


        im.converTobinary();
        im.write("test.bmp");
        im.crop(150,900,100,450);
        im.write("test2.bmp");
        im.findBarcode();
        im.scanCode().forEach((key, value) -> { 
            System.out.println("------------------------------------------------------------------");
            System.out.println("Barcode found: "+key);
            System.out.println("Decoded character is "+value);
        });
    }

}
    